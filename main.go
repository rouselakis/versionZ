package main

import (
	"github.com/ov3r1d3/versionZ/lib/version"
	"flag"
	"fmt"
)

var versionZ string

func main() {
	programVersion :=  new(version.Version)

	var versionFlag *string;

	// Parse the flags give at runtime
	if(versionZ == ""){
		versionFlag = flag.String("v",programVersion.ReadVersionFromFile(),"Set the version string")
	} else {
		versionFlag = flag.String("v",versionZ,"Set the version string")
	}

	jsonBool  := flag.Bool("j", false, "Show the format with json")
	flag.Parse()

	validVersion := programVersion.ParseString(*versionFlag)

	if(validVersion){
		if(*jsonBool){
			programVersion.ExportJson()
		} else {
			// Display a welcome Message
			WelcomeMessage()
			fmt.Println("Program Version is :")
			fmt.Println(programVersion.ReadVersionFromFile())
		}


	} else {
		fmt.Println("Errrr, theres something wrong")
	}
}


func WelcomeMessage(){
	fmt.Println("\nWelcome to VersionZ!\n")
}

func init(){

}