NAME
    VersionZ is a version control program.

AUTHORS
    Rouselakis Stylianos <rouselakis@gmail.com>

PARAMETERS

    No parameter : Show a message containing the program version

    -j Show the current version or the version that is set of the program in a json format ( e.g. {"version":"2.7.6"} )

    -v Set the version of the program ( e.g. -v=1.2.3 ) during run time.

    -ldflags "-X main.versionZ="XXX" Set the version of the program during build time.

    Full example :

    go run -ldflags "-X main.versionZ=2.7.9" main.go -j