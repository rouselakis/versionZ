package version

import (
	"fmt"
	"strconv"
	"encoding/json"
	"strings"
	"io/ioutil"
)

type Version struct {
	Major int
	Minor int
	Patch int
	Json struct {
		Version string `json:"version"`
	}
}

func (version *Version) Init(major int, minor int, patch int) {
	version.Major = major
	version.Minor = minor
	version.Patch = patch
	version.Json.Version = strconv.Itoa(version.Major)+"." + strconv.Itoa(version.Minor)+"."+strconv.Itoa(version.Patch)
	writeVersionToFile(version.Json.Version)
}

func (version *Version) PrintVersion(){
	fmt.Println("Current Program Version: ")
	fmt.Println(version.ReadVersionFromFile())
	//fmt.Println(version.Json.Version)
}

func (version *Version)SetVersion(major int,minor int,patch int){
	version.Init(major,minor,patch)
}


func(version *Version) ParseString(versionName string)(valid bool){
	splitVersion := strings.Split(versionName,".")
	major, err_major := strconv.Atoi(splitVersion[0])
	minor, err_minor := strconv.Atoi(splitVersion[1])
	patch, err_patch := strconv.Atoi(splitVersion[2])
	if(err_major != nil || err_major != err_minor || err_major != err_patch){
		fmt.Println("Please give a valid version number!\n")
		if(err_major !=nil){
			fmt.Println(splitVersion[0], " is not an integer")
		}
		if(err_minor !=nil){
			fmt.Println(splitVersion[1], " is not an integer")
		}
		if(err_patch !=nil){
			fmt.Println(splitVersion[2], " is not an integer")
		}
		fmt.Println("")
	} else {
		valid = true
	}
	version.Init(major,minor,patch)
	return valid
}

func (version *Version)ExportJson(){
	b, err := json.Marshal(version.Json);
	if(err != nil){
		fmt.Println(err)
	}
	fmt.Println(string(b[:]));
}


func writeVersionToFile(versionString string){
	ioutil.WriteFile("version", []byte(versionString), 0644)
}

func(version *Version) ReadVersionFromFile()(versionString string){
	versionFile, err := ioutil.ReadFile("version")
	if err != nil {
		panic(err)
	}
	return string(versionFile[:]);
}

